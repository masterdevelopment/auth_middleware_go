package auth

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

// RequestChannel .
var RequestChannel chan Request

// ValidationHandler for the new services that dont rely on mux to route
func ValidationHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		jtoken := GenerateJwtToken()

		if IsDebug() {
			w.Header().Set("application-token", jtoken.Token)
			next.ServeHTTP(w, r)
			return
		}

		if pathStartsWith("ignored_paths", r.URL.Path) {
			w.Header().Set("application-token", jtoken.Token)
			next.ServeHTTP(w, r)
			return
		}

		// The key and name env vars must be set
		if !hasKey() || !hasName() {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(503)
			json.NewEncoder(w).Encode(AError{Message: "Application has not been setup properly.", Code: 503})
			return
		}

		// Grabs the token from the request header and verifies that it has the jwt token
		token := r.Header.Get("application-token")
		if token == "" {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(511)
			json.NewEncoder(w).Encode(AError{Message: "An authorization header is required", Code: 511})
			return
		}
		jwt := &JwtToken{Token: token}

		// Verifies that the token is valid and returns us the app_name
		verify := jwt.VerifyAuth()
		if verify["is_valid"] == false {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(AError{Message: "Unauthorized request", Code: 401})
			return
		}

		// Checks that the application is authorized to make requests
		isAuthorized := ApplicationIsAuthorized(verify["app_name"].(string))
		if isAuthorized["is_authorized"] == false {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(AError{Message: isAuthorized["message"].(string), Code: 401})
			return
		}

		// Runs the handler function and sets a application-token for the response header
		w.Header().Set("application-token", jtoken.Token)
		next.ServeHTTP(w, r)
	})
}

// ValidationMiddleware for the services that rely on mux
func ValidationMiddleware(fnc http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		jtoken := GenerateJwtToken()
		// If the application is set to debug doesn't validate anything and just sends the
		// request through
		if IsDebug() {
			w.Header().Set("application-token", jtoken.Token)
			fnc(w, req)
			return
		}

		// The key and name env vars must be set
		if !hasKey() || !hasName() {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(503)
			json.NewEncoder(w).Encode(AError{Message: "Application has not been setup properly.", Code: 503})
			return
		}

		// Grabs the token from the request header and verifies that it has the jwt token
		token := req.Header.Get("application-token")
		if token == "" {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(511)
			json.NewEncoder(w).Encode(AError{Message: "An authorization header is required", Code: 511})
			return
		}
		jwt := &JwtToken{Token: token}

		// Verifies that the token is valid and returns us the app_name
		verify := jwt.VerifyAuth()
		if verify["is_valid"] == false {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(AError{Message: "Unauthorized request", Code: 401})
			return
		}

		// Checks that the application is authorized to make requests
		isAuthorized := ApplicationIsAuthorized(verify["app_name"].(string))
		if isAuthorized["is_authorized"] == false {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(AError{Message: isAuthorized["message"].(string), Code: 401})
			return
		}

		// Runs the handler function and sets a application-token for the response header
		w.Header().Set("application-token", jtoken.Token)
		fnc(w, req)
	})
}

// VerifyAuthentication for the services that rely on mux to run the middleware
func VerifyAuthentication(fnc http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		RequestChannel = make(chan Request)
		var request Request
		defer req.Body.Close()
		_ = json.NewDecoder(req.Body).Decode(&request)

		// The key and name env vars must be set
		if !hasKey() || !hasName() {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(503)
			json.NewEncoder(w).Encode(AError{Message: "Application has not been setup properly.", Code: 503})
			return
		}

		// Grabs the token from the request header and verifies that it has the jwt token
		token := req.Header.Get("Authorization")
		if token == "" || len(strings.Split(token, " ")) < 2 {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(511)
			json.NewEncoder(w).Encode(AError{Message: "An authorization header is required", Code: 511})
			return
		}
		jwt := &JwtToken{Token: strings.Split(token, " ")[1]}

		// Runs the jwt verification method
		uid, _ := strconv.Atoi(request.UserID)
		valid := jwt.VerifyUser(uid)
		if valid["is_valid"] == false {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(AError{Message: valid["message"].(string), Code: 401})
			return
		}
		ctx := req.Context()
		ctx = context.WithValue(ctx, ContextSessionKey, valid["sessionKey"])
		// If everything is correct sends the request data through the RequestChannel
		go func() { RequestChannel <- request }()

		fnc(w, req.WithContext(ctx))
	})
}

func pathStartsWith(filePath, url string) bool {
	// Opens the file and gets all its contents
	file, _ := ioutil.ReadFile(filePath)
	paths := strings.Split(string(file), "\n")

	// Iterates through the file checking if the url starts with any of the paths
	// in the file
	for _, path := range paths {
		if path == "" {
			continue
		}
		if strings.HasPrefix(url, path) {
			return true
		}
	}
	return false
}
