package auth

import (
	"database/sql"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var checkdb *sql.DB
var app = &Application{Name: "Test Check", CanRequest: true}

func initCheckTests() {
	// Initializes a new database for this set of tests
	_ = InitDB("postgres://postgres:Wblake91@10.232.6.1:5432/auth_middleware_go?sslmode=disable")
	checkdb = AuthDatabase
	os.Setenv("APP_NAME", "test_check")
	app.CreateTable(checkdb)
	app.CleanTable(checkdb)

	//Insert test auth app
	app.Insert(checkdb)
}

func TestApplicationIsAuthorized(t *testing.T) {
	initCheckTests()
	// Sets the authdatabase global to the database created for this tests
	AuthDatabase = checkdb
	// Sends the app.Name as parameter as it's the value that was inserted on the init function
	response := ApplicationIsAuthorized(app.Name)

	// After using the ApplicationIsAuthorized method, checks that the response returns true
	// and no error message
	assert.NotEqual(t, nil, response)
	assert.Equal(t, true, response["is_authorized"])
	assert.Equal(t, "", response["message"])
}

func TestApplicationIsNotAuthorized(t *testing.T) {
	// Sets the authdatabase global to the database created for this tests
	AuthDatabase = checkdb
	// Sends a dummy application name to check if its authorized
	response := ApplicationIsAuthorized("Not Authorized")

	// The application name sent doesn't exist thus it should yield a result saying that the
	// application isn't authorized
	assert.NotEqual(t, nil, response)
	assert.Equal(t, false, response["is_authorized"])
	assert.Equal(t, "Unauthorized request", response["message"])
}

func TestResponseIsTrusted(t *testing.T) {
	// Sets the authdatabase global to the database created for this tests
	AuthDatabase = checkdb
	// Sends the app.Name as parameter as it's the value that was inserted on the init function
	response := ResponseIsTrusted(app.Name)

	// The application name exists on the database so the result must be true
	assert.NotEqual(t, nil, response)
	assert.Equal(t, true, response["is_authorized"])
}

func TestResponseIsNotTrusted(t *testing.T) {
	// Sets the authdatabase global to the database created for this tests
	AuthDatabase = checkdb
	// Sends dummy parameter to check that it doesn't exist
	response := ResponseIsTrusted("Not Authorized")

	// Checks that the result in fact, says that the application isn't authorized
	assert.NotEqual(t, nil, response)
	assert.Equal(t, false, response["is_authorized"])
}
