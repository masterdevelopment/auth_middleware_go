package auth

import "fmt"

// AError custom struct
type AError struct {
	Message string
	Code    int
}

func (ae AError) Error() string {
	return fmt.Sprintf("%v", ae.Message)
}
