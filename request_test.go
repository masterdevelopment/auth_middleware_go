package auth

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"net/http"
	"os"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

var requestdb *sql.DB

func createTestEndpoint() {
	r := mux.NewRouter()
	// Creates the application on a database called test_request.db
	application := Application{CanRequest: true, Name: "test_requests"}
	InitDB("postgres://postgres:Wblake91@10.232.6.1:5432/auth_middleware_go?sslmode=disable")
	requestdb = AuthDatabase
	application.CreateTable(requestdb)
	application.Insert(requestdb)
	// Create the endpoint setting it to the  /test URL with a GET, POST, PUT and DELETE methods
	r.HandleFunc("/test", ValidationMiddleware(func(w http.ResponseWriter, r *http.Request) {
		token := GenerateJwtToken()
		w.Header().Set("application-token", token.Token)
		w.Header().Set("Content-Type", "application/json")

		json.NewEncoder(w).Encode(application)
		return
	})).Methods("GET", "POST", "PUT", "DELETE")

	r.HandleFunc("/auth", VerifyAuthentication(func(w http.ResponseWriter, r *http.Request) {
		payload := <-RequestChannel
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(payload)
		return
	}))

	// Start the test endpoint server
	go http.ListenAndServe(":12345", r)
}

func initRequestTests() {
	os.Setenv("APP_KEY", "secret")
	os.Setenv("APP_NAME", "test_requests")
	os.Setenv("APP_DEBUG", "")
	createTestEndpoint()
}

func TestExternalGetRequest(t *testing.T) {
	initRequestTests()
	// Runs a get request with the external parameter set to true to a dummy endpoint
	header := http.Header{}
	resp, err := Get(true, "http://example.com/", header, nil)

	// Doesn't do any validation check so the response should be OK
	assert.Equal(t, nil, err)
	assert.Equal(t, 200, resp.StatusCode)
}

func TestExternalPostRequest(t *testing.T) {
	// Runs a post request with the external parameter set to true to a dummy endpoint
	header := http.Header{}
	resp, err := Post(true, "http://example.com/", header, nil)

	// Doesn't do any validation check so the response should be OK
	assert.Equal(t, nil, err)
	assert.Equal(t, 200, resp.StatusCode)
}

func TestExternalPutRequest(t *testing.T) {
	// Runs a put request with the external parameter set to true to a dummy endpoint
	header := http.Header{}
	resp, err := Put(true, "http://example.com/", header, nil)

	// Doesn't do any validation check so the response should be OK or method not allowed
	assert.Equal(t, nil, err)
	assert.Equal(t, 405, resp.StatusCode)
}

func TestExternalDeleteRequest(t *testing.T) {
	// Runs a delete request with the external parameter set to true to a dummy endpoint
	header := http.Header{}
	resp, err := Delete(true, "http://example.com/", header, nil)

	// Doesn't do any validation check so the response should be OK or method not allowed
	assert.Equal(t, nil, err)
	assert.Equal(t, 405, resp.StatusCode)
}

func TestAuthInvalidRequest(t *testing.T) {
	// Sets the authdatabase global to the database created for this tests
	AuthDatabase = requestdb
	// Runs a get request with the external parameter set to false to a dummy endpoint
	header := http.Header{}
	resp, err := Get(false, "http://example.com/", header, nil)

	// It should fail because the response doesn't have the correct header
	assert.Equal(t, "Response is coming from an untrusted source.", err.Error())
	assert.Equal(t, 599, resp.StatusCode)
}

func TestValidRequest(t *testing.T) {
	// Sets the authdatabase global to the database created for this tests
	AuthDatabase = requestdb

	header := http.Header{}
	resp, err := Get(false, "http://localhost:12345/test", header, nil)

	assert.Equal(t, nil, err)
	assert.Equal(t, 200, resp.StatusCode)
}

func TestVerifyAuthentication(t *testing.T) {
	// Sets the authdatabase global to the database created for this tests
	AuthDatabase = requestdb
	client := &http.Client{}
	// Creates a new jwt token and a payload to be authenticated
	jwt := GenerateUserJwt(100)
	payload := Request{
		Method:     "POST",
		Service:    "users",
		IsExternal: false,
		Path:       "/test",
		UserID:     "100",
	}
	reqBodyBytes := new(bytes.Buffer)
	json.NewEncoder(reqBodyBytes).Encode(payload)

	// Creates a new request object with the payload and the header
	req, _ := http.NewRequest("POST", "http://localhost:12345/auth", reqBodyBytes)
	req.Header.Set("Authorization", "Bearer "+jwt.Token)

	// Runs the request with the client created previously
	resp, err := client.Do(req)

	assert.Equal(t, nil, err)
	assert.Equal(t, 200, resp.StatusCode)
}
