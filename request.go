package auth

import (
	"bytes"
	"encoding/gob"
	"io/ioutil"
	"net/http"
	"time"
)

// Get - sends a get request to the desired endpoint and if the endpoint isn't external
// validates that the response must have a token, returns the response object and an error
func Get(isExternal bool, url string, headers http.Header, body []byte) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	if !isExternal {
		// Adds the application token request header
		headers = appendHeader(headers)
	}
	req.Header = headers
	req.Close = true
	return makeRequest(isExternal, req)
}

// Post - sends a post request to the desired endpoint and if the endpoint isn't external
// validates that the response must have a token, returns the response object and an error
func Post(isExternal bool, url string, headers http.Header, body []byte) (*http.Response, error) {
	var req *http.Request
	var err error
	if body != nil {
		req, err = http.NewRequest("POST", url, bytes.NewBuffer(body))
	} else {
		req, err = http.NewRequest("POST", url, nil)
	}

	if err != nil {
		return nil, err
	}
	if !isExternal {
		// Adds the application token request header
		headers = appendHeader(headers)
	}
	req.Header = headers
	req.Close = true
	return makeRequest(isExternal, req)
}

// Put - sends a put request to the desired endpoint and if the endpoint isn't external
// validates that the response must have a token, returns the response object and an error
func Put(isExternal bool, url string, headers http.Header, body []byte) (*http.Response, error) {
	var req *http.Request
	var err error

	if body != nil {
		req, err = http.NewRequest("PUT", url, bytes.NewBuffer(body))
	} else {
		req, err = http.NewRequest("PUT", url, nil)
	}

	if err != nil {
		return nil, err
	}

	if !isExternal {
		// Adds the application token request header
		headers = appendHeader(headers)
	}
	req.Header = headers
	req.Close = true
	return makeRequest(isExternal, req)
}

// Delete - sends a delete request to the desired endpoint and if the endpoint isn't external
// validates that the response must have a token, returns the response object and an error
func Delete(isExternal bool, url string, headers http.Header, body []byte) (*http.Response, error) {
	var req *http.Request
	var err error
	if body != nil {
		req, err = http.NewRequest("DELETE", url, bytes.NewBuffer(body))
	} else {
		req, err = http.NewRequest("DELETE", url, nil)
	}

	if err != nil {
		return nil, err
	}

	if !isExternal {
		// Adds the application token request header
		headers = appendHeader(headers)
	}
	req.Header = headers
	req.Close = true
	return makeRequest(isExternal, req)
}

// appendHeader - appends the application-token header
func appendHeader(headers http.Header) http.Header {
	jwt := GenerateJwtToken()
	headers.Set("application-token", jwt.Token)
	return headers
}

// makeRequest - sends http request using the request object provided, and if it isn't external
// validates the response to be from a secure source
func makeRequest(isExternal bool, req *http.Request) (*http.Response, error) {
	// Creates a client object so we can send our request
	client := &http.Client{
		Timeout: 30 * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if !isExternal {
		// If it isn't external we check that the response comes from a secure source
		resp, err = processResponseHeader(resp)
	}

	return resp, err
}

// processResponseHeader - validates that the response comes from a secure source
func processResponseHeader(response *http.Response) (*http.Response, error) {
	if IsDebug() {
		return response, nil
	}

	token := response.Header.Get("application-token")
	if token == "" {
		// If the response doesn't have a token it's not from a secure source
		return untrustedResponse(), AError{Message: "Response is coming from an untrusted source.", Code: 599}
	}
	// Create a jwtToken object to then verify it
	jwtToken := &JwtToken{Token: token}
	tokenVerified := jwtToken.VerifyAuth()

	// Check that its a valid token
	if tokenVerified["is_valid"] == false {
		return untrustedResponse(), AError{Message: tokenVerified["message"].(string), Code: 599}
	}

	// Checks that the app name is on the trusted applications for the auth middleware
	isTrusted := ResponseIsTrusted(tokenVerified["app_name"].(string))

	if isTrusted["is_authorized"] == false {
		return untrustedResponse(), AError{Message: "Untrusted source " + tokenVerified["app_name"].(string), Code: 599}
	}
	return response, nil
}

// untrustedResponse - returns an untrusted response object
func untrustedResponse() *http.Response {
	body := make(map[string]string)
	body["message"] = "Response is coming from an untrusted source."
	bodyBytes, _ := getBytes(body)

	header := http.Header{}
	header.Set("Content-Type", "application/json;UTF-8;")
	res := &http.Response{
		StatusCode:    599,
		Body:          ioutil.NopCloser(bytes.NewBuffer(bodyBytes)),
		ContentLength: int64(len(body)),
		Header:        header,
	}
	return res
}

// getBytes - convert any datatype to a byte array
func getBytes(data interface{}) ([]byte, error) {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)
	err := enc.Encode(data)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
