package auth

// Key Context types
type Key int

// Context enums
const (
	ContextSessionKey Key = iota
	ContextRequestPayload
)
