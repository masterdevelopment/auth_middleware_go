package auth

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func initJwtTests() {
	os.Setenv("APP_NAME", "api_gateway")
	os.Setenv("APP_KEY", "secret")
}

func TestGenerateJwtToken(t *testing.T) {
	initJwtTests()
	// Generate jwt token with the required, default data
	token := GenerateJwtToken()
	// Validate that the generated token is not nil and that the token.Token is not an empty string
	assert.NotNil(t, token)
	assert.NotEqual(t, "", token.Token)
}

func TestValidateToken(t *testing.T) {
	// Generates new token to validate afterwards
	token := GenerateJwtToken()
	// Runs the verify method and expects a map with an app_name and is_valid = true
	res := token.VerifyAuth()

	assert.True(t, res["is_valid"].(bool))
	assert.Equal(t, os.Getenv("APP_NAME"), res["app_name"].(string))
}

func TestGenerateUserJwt(t *testing.T) {
	// Generate jwt token with the required, default data
	token := GenerateUserJwt(2011102)
	// Validate that the generated token is not nil and that the token.Token is not an empty string
	assert.NotNil(t, token)
	assert.NotEqual(t, "", token.Token)
}

func TestValidateUserJwt(t *testing.T) {
	// Generate jwt token with the required, default data
	token := GenerateUserJwt(2011102)
	// Runs the verify user method and expects a map with is_valid = true
	res := token.VerifyUser(2011102)

	assert.True(t, res["is_valid"].(bool))
}
