package auth

import (
	"database/sql"
	"os"

	// Sets the database driver to postgres
	_ "github.com/lib/pq"
)

// Application data service
type Application struct {
	Name       string
	CanRequest bool
}

// AuthDatabase object
var AuthDatabase *sql.DB

// InitDB init the sqlite3 db
func InitDB(connection string) error {
	db, err := sql.Open("postgres", connection)
	if err != nil {
		return err
	}
	if db == nil {
		return AError{Message: "Couldn't connect to the database", Code: 500}
	}
	AuthDatabase = db
	return nil
}

// CreateTable creates the new table if it doesnt exist
func (a *Application) CreateTable(db *sql.DB) error {
	sqlTable := `
	CREATE TABLE IF NOT EXISTS ` + os.Getenv("APP_NAME") + `(
		name TEXT PRIMARY KEY,
		can_request INTEGER DEFAULT(1)
	);
	`

	_, err := db.Exec(sqlTable)
	return err
}

// CleanTable deletes all the rows from the applications table
func (a *Application) CleanTable(db *sql.DB) error {
	sqlClean := "DELETE FROM " + os.Getenv("APP_NAME") + ""

	_, err := db.Exec(sqlClean)
	return err
}

// Get gets an application looking for its name
func (a *Application) Get(db *sql.DB) (*Application, error) {
	sqlGet := "SELECT name, can_request FROM " + os.Getenv("APP_NAME") + " WHERE name=$1"

	row, err := db.Query(sqlGet, a.Name)
	if err != nil {
		return &Application{}, err
	}
	defer row.Close()

	for row.Next() {
		item := Application{}
		err2 := row.Scan(&item.Name, &item.CanRequest)
		if err2 != nil {
			return &Application{}, err2
		}
		return &item, nil
	}
	return &Application{}, nil
}

// GetAll gets all the applications
func (a *Application) GetAll(db *sql.DB) ([]Application, error) {
	sqlGetAll := "SELECT name, can_request FROM " + os.Getenv("APP_NAME") + ""

	rows, err := db.Query(sqlGetAll)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []Application
	for rows.Next() {
		item := Application{}
		err2 := rows.Scan(&item.Name, &item.CanRequest)
		if err2 != nil {
			return nil, err2
		}
		result = append(result, item)
	}
	return result, nil
}

// Insert add a new application
func (a *Application) Insert(db *sql.DB) error {
	sqlInsert := `
	INSERT INTO ` + os.Getenv("APP_NAME") + ` 
	("name", can_request)
	VALUES($1, $2);
	`
	if !a.CanRequest {
		_, err := db.Exec(sqlInsert, a.Name, 0)
		return err
	}
	_, err := db.Exec(sqlInsert, a.Name, 1)
	return err
}

// Update update a row by its name
func (a *Application) Update(db *sql.DB) error {
	sqlUpdate := `
	UPDATE ` + os.Getenv("APP_NAME") + ` 
	SET can_request = $1 
	WHERE name = $2
	`
	if !a.CanRequest {
		_, err := db.Exec(sqlUpdate, 0, a.Name)
		return err
	}
	_, err := db.Exec(sqlUpdate, 1, a.Name)
	return err
}
