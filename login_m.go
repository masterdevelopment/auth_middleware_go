package auth

// Login DTO
type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

// LoginRequest DTO
type LoginRequest struct {
	Data LoginData `json:"data"`
}

// LoginData DTO
type LoginData struct {
	Type       string          `json:"type"`
	Attributes LoginAttributes `json:"attributes"`
}

// LoginAttributes DTO
type LoginAttributes struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// LoginResponse DTO
type LoginResponse struct {
	Data LoginResponseData `json:"data"`
}

// LoginResponseData DTO
type LoginResponseData struct {
	Message       string `json:"message"`
	UserID        int    `json:"user_id"`
	LeadershipID  int    `json:"leadership_id"`
	PaymentStatus int    `json:"payment_status"`
}

// CreateLoginRequest creates a new LoginRequest object
func CreateLoginRequest(username, password string) LoginRequest {
	return LoginRequest{
		Data: LoginData{
			Type: "User",
			Attributes: LoginAttributes{
				Username: username,
				Password: password,
			},
		},
	}
}
