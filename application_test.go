package auth

import (
	"database/sql"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var db *sql.DB
var a = &Application{Name: "Test App", CanRequest: false}

func initAppTests() {
	_ = InitDB("postgres://postgres:Wblake91@10.232.6.1:5432/auth_middleware_go?sslmode=disable")
	db = AuthDatabase
	os.Setenv("APP_NAME", "test")
	a.CreateTable(db)
	a.CleanTable(db)
}

func TestInsert(t *testing.T) {
	initAppTests()
	// Runs an insert using the a global variable
	err := a.Insert(db)
	assert.Equal(t, nil, err)

	// Fetches all the apps in the table
	apps, errg := a.GetAll(db)
	assert.Equal(t, nil, errg)

	// apps should be of lenght 1 because we've only inserted 1 thus far
	assert.Len(t, apps, 1)
}

func TestGet(t *testing.T) {
	app, err := a.Get(db)

	assert.Equal(t, "Test App", app.Name)
	assert.Equal(t, nil, err)
}

func TestUpdate(t *testing.T) {
	// Updates the value of CanRequest from false to true
	a.CanRequest = true
	err := a.Update(db)
	assert.Equal(t, nil, err)

	// Fetches the object with a get request
	app, errg := a.Get(db)
	// Checks that the new object has the right value on CanRequest
	assert.Equal(t, true, app.CanRequest)
	assert.Equal(t, nil, errg)
}
