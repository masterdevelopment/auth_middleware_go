package auth

// Request DTO
type Request struct {
	Method     string      `json:"method"`
	Service    string      `json:"service"`
	Path       string      `json:"path"`
	Endpoint   string      `json:"endpoint"`
	IsExternal bool        `json:"is_external"`
	UserID     string      `json:"user_id"`
	Role       string      `json:"role"`
	Payload    interface{} `json:"payload"`
}
