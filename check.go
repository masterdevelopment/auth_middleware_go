package auth

// ApplicationIsAuthorized - Verifies that the application is authorized to make requests
func ApplicationIsAuthorized(appName string) map[string]interface{} {
	response := make(map[string]interface{})
	app := &Application{Name: appName}

	response["is_authorized"] = false
	app, err := app.Get(AuthDatabase)
	// Checks that we found the app in the database and that it can request
	// otherwise returns returns a map with the is_authorized = false
	if err != nil || app.Name == "" || !app.CanRequest {
		response["message"] = "Unauthorized request"
		return response
	}

	response["is_authorized"] = true
	response["message"] = ""
	return response
}

// ResponseIsTrusted - checks that the response is comming from a trusted source
func ResponseIsTrusted(appName string) map[string]interface{} {
	response := make(map[string]interface{})
	app := &Application{Name: appName}

	response["is_authorized"] = true
	app, err := app.Get(AuthDatabase)

	if err != nil || app.Name == "" {
		response["is_authorized"] = false
		return response
	}
	return response
}
