package auth

import (
	"fmt"
	"os"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// AuthClaims access model
type AuthClaims struct {
	AppName string    `json:"app_name"`
	Exp     time.Time `json:"exp"`
	Iat     time.Time `json:"iat"`
	Nbf     time.Time `json:"nbf"`
}

// JwtToken DTO
type JwtToken struct {
	Token string
}

// GenerateJwtToken - Generates a new json web token with expiration of 10 seconds after the creation time
// and the app_name attribute as the name of the current application, from the env variables
func GenerateJwtToken() *JwtToken {
	env := os.Getenv("APP_EXPIRATION_TIME")
	if env == "" {
		env = "10"
	}
	exp, _ := strconv.Atoi(env)

	token := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"app_name": GetName(),
		"exp":      time.Now().UTC().Add(time.Second * time.Duration(exp)).Unix(),
		"iat":      time.Now().Unix(),
		"nbf":      time.Now().Unix(),
	})

	// Signs the token with the key on APP_KEY env variable
	tokenString, err := token.SignedString([]byte(GetKey()))
	if err != nil {
		fmt.Println(err)
	}

	// Creates a new JwtToken object with the token generated previously
	jwtToken := &JwtToken{Token: tokenString}
	return jwtToken
}

// GenerateUserJwt .
func GenerateUserJwt(userID int, key string) *JwtToken {
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
		"user_id":    userID,
		"exp":        time.Now().UTC().Add(time.Minute * 30),
		"iat":        time.Now().UTC(),
		"nbf":        time.Now().UTC(),
		"sessionKey": key,
	})

	tokenString, err := token.SignedString([]byte(GetKey()))
	if err != nil {
		fmt.Println(err)
	}

	// Creates a new JwtToken object with the token generated previously
	jwtToken := &JwtToken{Token: tokenString}
	return jwtToken
}

// VerifyAuth - checks all the basic validations that the jwt token must have, being able
// to decode it, not yielding an error while decoding it, and the basic validations with
// the token's claims
func (j *JwtToken) VerifyAuth() map[string]interface{} {
	response := make(map[string]interface{})
	response["is_valid"] = false
	// Decodes the token using the key to get its value
	token, err := j.decodeToken()
	// Checks that the decoding yielded no errors
	if err != nil {
		response["message"] = err.Error()
		return response
	}

	// Checks that the token is valid and converts the claims to MapClaims
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok && !token.Valid {
		// If the token isn't valid it returns an "Invalid token" message
		response["message"] = "Invalid token"
		return response

	}

	// Validates the claims on the payload (exp, iat, nbf and app_name)
	valid, msg := validateBasicClaimsUnix(claims)
	if !valid || claims["app_name"] == "" {
		response["message"] = msg
		return response
	}

	// Validations where succesful and the response object gets the is_valid set
	// as true and the app_name as the app_name from the claims
	response["is_valid"] = true
	response["app_name"] = claims["app_name"]

	return response
}

// VerifyUser .
func (j *JwtToken) VerifyUser(userID int) map[string]interface{} {
	response := make(map[string]interface{})
	response["is_valid"] = false
	// Decodes the token using the key to get its value
	token, err := j.decodeToken()
	// Checks that the decoding yielded no errors
	if err != nil {
		response["message"] = err.Error()
		return response
	}

	// Checks that the token is valid and converts the claims to MapClaims
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok && !token.Valid {
		// If the token isn't valid it returns an "Invalid token" message
		response["message"] = "Invalid token"
		return response
	}

	// Validates the claims on the payload (exp, iat, nbf and user_id)
	valid, msg := validateBasicClaims(claims)
	if !valid || claims["user_id"].(float64) != float64(userID) {
		response["message"] = msg
		return response
	}

	// Validations where succesful and the response object gets the is_valid set
	// to true
	response["is_valid"] = true
	response["sessionKey"] = claims["sessionKey"]
	return response
}

func (j *JwtToken) decodeToken() (*jwt.Token, error) {
	return jwt.Parse(j.Token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unable to decode the token")
		}
		return []byte(GetKey()), nil
	})
}

// validateBasicClaimsUnix - validates all of the claims properties for it to be a valid token with unix datetime
func validateBasicClaimsUnix(claims jwt.MapClaims) (bool, string) {
	// validates that the claims map has exp, iat, nbf and app_name
	if claims["exp"] == nil || claims["iat"] == nil || claims["nbf"] == nil {
		return false, "Invalid token"
	}
	// Iterates through all the keys on the mapClaims
	for key, value := range claims {
		switch key {
		// Checks that the token hasnt expired (exp > time.Now())
		case "exp":
			if time.Now().Unix() < value.(int64) {
				return false, "Expired token"
			}
		}
	}
	return true, ""
}

// validateBasicClaims - validates all of the claims properties for it to be a valid token
func validateBasicClaims(claims jwt.MapClaims) (bool, string) {
	// validates that the claims map has exp, iat, nbf and app_name
	if claims["exp"] == nil || claims["iat"] == nil || claims["nbf"] == nil {
		return false, "Invalid token"
	}
	// Iterates through all the keys on the mapClaims
	for key, value := range claims {
		// Tries to convert the map value
		val := fmt.Sprintf("%v", value)
		date, err := time.Parse(time.RFC3339, val)
		switch key {
		// Checks that the token hasnt expired (exp > time.Now())
		case "exp":
			if err != nil {
				v := value.(float64)
				if int64(v) >= time.Now().Unix() {
					continue
				}
			}
			if !validateDate(time.Now(), date) {
				return false, "Expired token"
			}
		// Checks that iat and nbf are before current time (iat && nbf > time.Now())
		case "iat", "nbf":
			if !validateDate(date, time.Now()) {
				return false, "Invalid token"
			}
		}
	}
	return true, ""
}

func validateDate(low, high time.Time) bool {
	// Converts the date objects to int64 and validates that low's value is lower
	// than high's
	return low.Unix() <= high.Unix()
}
